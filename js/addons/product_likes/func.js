(function (_, $) {
    var product_id = $('#product_id').val();

    $(document).ready(function() {

        fn_get_product_data(product_id);
    });

    function fn_get_product_data(product_id) {
        $.ceAjax('request', fn_url('product_likes.get_likes'), {
            hidden: true,
            data: {
                product_id: product_id,
            },
            result_ids: 'product_likes_div',
        });
    };

    $('#product_like_button').on('click', function() {
        $.ceAjax('request', fn_url('product_likes.like_product'), {
            hidden: true,
            data: { product_id: product_id },
            result_ids: 'product_likes_div',
        });
    });
}(Tygh, Tygh.$));
