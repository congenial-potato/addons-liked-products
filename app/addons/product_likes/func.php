<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_product_likes_get_product_likes_data($product_id)
{
    if (!empty($product_id)) {
        $product_likes = db_get_field('SELECT product_likes FROM ?:products WHERE product_id = ?i', $product_id);

        return $product_likes;
    }
}

function fn_product_likes_product_like($product_id, $auth)
{
    if (!empty($product_id)) {

        $product_liked_by_user = db_get_row('SELECT * FROM ?:product_likes WHERE product_id = ?i AND user_id = ?i', $product_id, $auth['user_id']);

       /* if (!empty($product_liked_by_user)) {
            return false;
        } else {*/
            $product_likes = fn_product_likes_get_product_likes_data($product_id);
            $new_product_rating = $product_likes + 1;
            //db_query('INSERT IGNORE INTO ?:product_likes (user_id, product_id, status) VALUES (?i, ?i, ?s)', $auth['user_id'], $product_id, 'Y');
            db_query('UPDATE ?:products SET product_likes = ?i WHERE product_id = ?i', $new_product_rating, $product_id);

            return $new_product_rating;
        //}
    }
}
