<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

$params = $_REQUEST;

if ($mode == 'get_likes') {
    if (defined('AJAX_REQUEST') && !empty($params['product_id'])) {
        $product_likes = fn_product_likes_get_product_likes_data($params['product_id']);

        Tygh::$app['view']->assign('product_data', $product_data);
        exit;
    }
} elseif ($mode == 'like_product') {
    if ($auth['user_id'] != 0) {
        if (defined('AJAX_REQUEST')) {fn_print_r('TEST');
            $product_likes = fn_product_likes_product_like($params['product_id'], $auth);

            if (!empty($product_likes)) {
                $product['product_likes'] = $product_likes;
                Tygh::$app['view']->assign('product', $product);
            } else {
                fn_set_notification('W', __('notice'), ('Вы уже оценили этот товар!'));
            }

        }
    } else {
        fn_set_notification('E', __('error'), ('Вы не авторизованы!'));
    }

    return array(CONTROLLER_STATUS_OK, "products.view");
}

