<?php
/**
 * The add-on creates symbolic links from current directory
 * To delete symbolic links, add "delete" to the end of the request
 */
if (empty($argv[1])) {
    echo 'You didn\'t specify the target directory, use: php create_symlinks.php ../../435 [ delete ]' . PHP_EOL;
    exit(127);
}

// source => target
$paths = array(
    '/app/addons/product_likes' => '/app/addons/product_likes',
    '/var/themes_repository/responsive/templates/addons/product_likes' => '/design/themes/responsive/templates/addons/product_likes',
    '/js/addons/product_likes' => '/js/addons/product_likes',
    '/var/langs/en/addons/product_likes.po' => '/var/langs/en/addons/product_likes.po',
    '/var/langs/ru/addons/product_likes.po' => '/var/langs/ru/addons/product_likes.po',
);

$dir_target = realpath($argv[1]);

$dir = realpath(dirname(__FILE__) . '/../');

foreach ($paths as $source => $target) {

    if (file_exists($dir_target . $target)) {
        unlink($dir_target . $target);
    }
    if (!isset($argv[2]) || $argv[2] != 'delete') {
        // WIN
        if (DIRECTORY_SEPARATOR == '\\') {
            $dirSrc = $dir . str_replace('/', '\\', $source);
            $dirDest = $dir_target . str_replace('/', '\\', $target);
            exec('mklink /D ' . '"' . $dirDest . '"' . ' ' . '"' .  $dirSrc . '"' . ' ');
        }
        // NIX
        else {
            system("ln -s {$dir}{$source} {$dir_target}{$target}");
        }
    }
}
