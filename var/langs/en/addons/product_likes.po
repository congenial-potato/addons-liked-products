msgid ""
msgstr "Project-Id-Version: tygh\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Language-Team: English\n"
"Language: en_US"

msgctxt "Addons::name::product_likes"
msgid "Product likes"
msgstr "Product likes"

msgctxt "Addons::description::product_likes"
msgid "Like your favorite products"
msgstr "Like your favorite products"

msgctxt "Languages::product_likes.like_product"
msgid "Like"
msgstr "Like"
