{script src="js/addons/product_likes/func.js"}

<div id="product_likes_div">
    <button class="ty-btn cm-ajax" type="button" id="product_like_button">
        <input type="hidden" value="{$product.product_id}" id="product_id">
        <span>{__("product_likes.like_product")}</span>
        <span id="product_likes">{$product.product_likes|default:0}</span>
    </button>
<!--product_likes_div--></div>
